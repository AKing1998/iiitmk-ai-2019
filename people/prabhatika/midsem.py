# ===================
# Mid Sem Exam
# ===================
# - Please copy the file called `midsem.py` into your **OWN** folder.
# - From now on, everything must be done in your own copy of the file.
# - Edit the file to solve the problem stated in it.
# - Save it, then commit your code and push to your fork of the project.
# - Make sure the tests are passing. If tests fail, you can look at the tests and figure out what you did wrong.
# - Submit a merge request to the Master branch of the class project.
# - Make sure that rebase issues do not exist or give rebase permissions to reviewer while creating the merge request
# - Once you are sure you are done and want to submit, simply mention @theSage21 in the comments of the merge request.
# - The timestamp on this comment shows when you finished your exam. You must finish within the exam time limit.

# NOTE: If at any time you have questions, please open an issue in the class project

# ===================
# Statement
# ===================

# You must write a function that returns a location in a game of tic-tac-toe
# your agent must try to win as many games as possible
# The board is a tuple of 3 strings. For example the starting position is:
#     ('   ',
#      '   ',
#      '   ')
# If your agent decides to put your symbol in coordinates (0, 0) the board would look like:

#     ('x  ',
#      '   ',
#      '   ')

# Your agent must return two coordinates whenever it is called
# for example in order to make the move shown above your function
# must be something like this:

# def agent(board, your_symbol):
#     return 0, 0


# ===================
# Scoring
# ===================

# The scoring is simple.
# For every game you win, you get 1 point
# For everything else you get 0 point (lose a game/end game due to invalid move)
# There are two test cases for this problem
# Each test case has a single opponent algorithm your agent must try to win against.
# You can see the jobs in the merge request to see how your submission is doing

import random


def agent(board, your_symbol):
    positions = {}
    if your_symbol == "x":
        opponent = "o"
    else:
        opponent = "x"
    positions[your_symbol] = []
    positions[opponent] = []
    positions_left = []
    for i, row in enumerate(board):
        for j, sym in enumerate(row):
            if len(sym.strip()) > 0:
                positions[sym].append((i, j))
            else:
                positions_left.append((i, j))
    for i, row in enumerate(board):
        if " " not in row:
            continue
        if ("oo" in row or "o o" in row) or ("xx" in row or "x x" in row):
            empty_pos = row.find(" ")
            print("H : ", empty_pos)
            return i, empty_pos
    for i in range(len(board)):
        vertical_str = "".join([row[i] for row in board])
        print("vertical_str | ", vertical_str)
        if " " not in vertical_str:
            continue
        if ("oo" in vertical_str or "o o" in vertical_str) or (
            "xx" in vertical_str or "x x" in vertical_str
        ):
            empty_pos = vertical_str.find(" ")
            print("V : ", empty_pos)
            return empty_pos, i
    left_diag = "".join([row[i] for i, row in zip(range(len(board)), board)])
    right_diag = "".join(
        [row[len(board) - 1 - i] for i, row in zip(range(len(board)), board)]
    )
    print("left | ", left_diag)
    print("right | ", right_diag)
    if len(left_diag) == 2 and (
        ("oo" in left_diag or "o o" in left_diag)
        or ("xx" in left_diag or "x x" in left_diag)
    ):
        empty_pos = left_diag.find(" ")
        print("L : ", empty_pos)
        return empty_pos, empty_pos
    if len(right_diag) == 2 and (
        ("oo" in left_diag or "o o" in left_diag)
        or ("xx" in right_diag or "x x" in right_diag)
    ):
        print("R : ", right_diag)
        empty_pos = right_diag.find(" ")
        return empty_pos, len(board) - 1 - empty_pos
    move = random.sample(positions_left, 1)
    return move[0][0], move[0][1]
